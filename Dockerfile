FROM centos:centos7

ARG THREADS=4

RUN yum install -y --nogpgcheck git scl-utils cmake vim boost-devel epel-release centos-release-scl && \
    yum install -y --nogpgcheck pugixml-devel devtoolset-10 root && \
    yum install -y --nogpgcheck llvm-toolset-7.0

RUN yum install -y zlib zlib-devel libjpeg-turbo-devel
RUN yum install -y python-devel python3-devel
RUN pip3 install --user pandas matplotlib numpy

WORKDIR /software

COPY . /software/power_supply

ENV \
    BASH_ENV="/usr/bin/scl_enable" \
    ENV="/usr/bin/scl_enable" \
    PROMPT_COMMAND=". /usr/bin/scl_enable"

RUN echo -e "\n\
unset BASH_ENV PROMPT_COMMAND ENV\n\
source scl_source enable llvm-toolset-7.0\n\
" > /usr/bin/scl_enable

RUN cd /software/power_supply && \
    git submodule init && git submodule update && \
    source ./setup.sh && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j$THREADS
