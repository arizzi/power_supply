#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os, errno
import glob
import pathlib
import argparse 
from csv import writer
import yaml
mpl.rcParams['mathtext.default'] = 'regular'

####################
# Analyze function #
####################

def analyze(fileFolder, fileBaseName, labelData, configSingle):
    """Analisys

    :fileFolder: Folder where file is located
    :fileBaseName: Base name for file to be analyzed

    """

    fileSaveDir        = fileFolder + 'Images/'
    fileSaveName       = fileSaveDir + fileBaseName + '.png'
    fileSaveNameTransp = fileSaveDir + fileBaseName + 'transparent.png'
    fileName           = fileFolder + fileBaseName + '.csv'
    fileType           = 'None'

    try:
        os.makedirs(fileSaveDir)
    except FileExistsError:
        pass

    print("Reading file: ", fileName)
    df = pd.read_csv(fileName, sep="\t", index_col=False)
    #print(df)
    
    # Getting data
    TimeVec = np. array(df['Time']. tolist())
    Voltage = -np. array(df['V'    ]. tolist())
    Current = -np. array(df['I'   ]. tolist())*1e6

    fig, axVoltage = plt.subplots()
    axCurrent      = axVoltage.twinx()

    # Plotting data
    VoltagePlt , = axVoltage.plot(TimeVec, Voltage, '.', label = 'Voltage', color = 'blue')
    CurrentPlt , = axCurrent.plot(TimeVec, Current, '.', label = 'Current', color = 'orange')
    #Merged plot
    plt.figure('Total')
    plt.plot(TimeVec, Current, label = labelData)
    plt.figure(fig.number)

    axVoltage.set_ylim(configSingle.get('voltage').get('yLim'))
    axCurrent.set_ylim(configSingle.get('current').get('yLim'))
    if configSingle.get('voltage').get('yLog') is True:
        axVoltage.set_yscale('log')
    if configSingle.get('current').get('yLog') is True:
        axCurrent.set_yscale('log')

    # Title and labels
    axVoltage.set_xlabel(configSingle.get('xLabel'))
    axVoltage.set_ylabel(configSingle.get('voltage').get('yLabel'))
    axCurrent.set_ylabel(configSingle.get('current').get('yLabel'))
    plt.title(configSingle.get('title'))

    # Showing and saving
    fig.set_size_inches(configSingle.get('size'))
    plt.tight_layout()
    plt.grid()
    print("Saving ", fileSaveName)
    plt.savefig(fileSaveName,       transparent = False, dpi = 1000)
    plt.savefig(fileSaveNameTransp, transparent = True,  dpi = 1000)
    if configSingle.get('show') is False:
        plt.close()

########
# Main #
########

def main(args):
    """Analyze all the data in folder
    """
    # Opening configuration file and storing information
    with open(args.configuration,'r') as configFile:
        try:
            configDict = yaml.safe_load(configFile)
        except yaml.YAMLError as e:
            print(e)
    #Useful variables
    firstLine    = list()
    dfConfig     = pd.read_csv(args.datalist, sep=",", comment='#', index_col=False)
    fileList     = np.array(dfConfig['FileName'].tolist())
    labelData    = np.array(dfConfig['Label'   ].tolist())
    fullData     = pd.DataFrame()
    figTot       = plt.figure('Total')
    configSingle = configDict.get('plot').get('single')
    configMerge  = configDict.get('plot').get('merge')
    
    for vInd, vFile in enumerate(fileList):
        fileBaseName = pathlib.Path(vFile).stem
        firstLine.append(labelData[vInd])
        firstLine.append('')
        firstLine.append('')
        print('Analyzing file: '+fileBaseName)
        analyze(str(pathlib.Path(vFile).parent)+'/', fileBaseName, labelData[vInd], configSingle)
        fullData = pd.concat([fullData,pd.read_csv(vFile, sep='\t',index_col=False)],axis = 1)
    # Showing and saving
    plt.figure(figTot.number)
    figTot.set_size_inches(configMerge.get('size'))
    axTot = plt.gca()
    axTot.set_ylim(configMerge.get('yLim'))
    axTot.set_xlabel(configMerge.get('xLabel'))
    axTot.set_ylabel(configMerge.get('yLabel'))
    if configMerge.get('yLog') is True:
        axTot.set_yscale('log')
    plt.legend()
    plt.figure('Total')
    plt.title(configMerge.get('title'))
    plt.tight_layout()
    plt.grid()
    print("Saving ", 'Results/Merge.png')
    plt.savefig('Results/Merge.png',       transparent = False, dpi = 1000)
    plt.savefig('Results/Merge_Transparent.png', transparent = True,  dpi = 1000)
    if configDict.get('plot').get('merge').get('show') is False:
        plt.close()
    plt.show()

    csvFile   = open(os.path.join(args.outputdatadir,'Merge.csv'),'w')
    csvWriter = writer(csvFile,delimiter='\t')
    csvWriter.writerow(firstLine)
    csvFile.close()
    fullData.to_csv(os.path.join(args.outputdatadir,'Merge.csv'),mode='a',index=False,sep='\t')

###################
# Argument parser #
###################

parser = argparse.ArgumentParser(description='Plot of IV curve data using matplotlib and pandas')
parser.add_argument('-c' ,'--configuration',help = 'YAML configuration file'                               ,default = 'config.yaml', type = str  )
parser.add_argument('-d' ,'--datalist'     ,help = 'Filein csv format with data file list and description' ,default = 'list.csv'   , type = str  )
parser.add_argument('-o' ,'--outputdatadir',help = 'Output directory for saving merged results'            ,default = 'Results/'   , type = str  )

args = parser.parse_args()

if __name__ == "__main__":
    main(args)
