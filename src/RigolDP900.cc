#include "RigolDP900.h"
#include "EthernetConnection.h"
#include "SerialConnection.h"
#include <unistd.h>   
#include <iostream>
#include <string>

RigolDP900::RigolDP900(const pugi::xml_node configuration) : PowerSupply("RigolDP900", configuration) { configure(); }
RigolDP900::~RigolDP900()
{
    if(fConnection != nullptr) delete fConnection;
}

void RigolDP900::configure()
{
    std::cout << "Configuring RigolDP900 ..." << std::endl;
    std::string connectionType = fConfiguration.attribute("Connection").as_string();
    int         timeout        = fConfiguration.attribute("Timeout").as_int();
    std::cout << connectionType << " connection ..." << std::endl;

    if(std::string(fConfiguration.attribute("Connection").value()).compare("Ethernet") == 0)
    {
        fConnection = new SharedEthernetConnection(fConfiguration.attribute("IPAddress").value(), std::stoi(fConfiguration.attribute("Port").value()));
    }
    else
    {
        std::stringstream error;
        error << "RigolDP900: Cannot implement connection type " << fConfiguration.attribute("Connection").value() << ".\n"
              << "Possible values are Serial or Ethernet";
        throw std::runtime_error(error.str());
    }

    for(pugi::xml_node channel = fConfiguration.child("Channel"); channel; channel = channel.next_sibling("Channel"))
    {
        std::string inUse = channel.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string id = channel.attribute("ID").value();
        std::cout << __PRETTY_FUNCTION__ << "Configuring channel: " << id << std::endl;
        PowerSupply::fChannelMap.emplace(id, new RigolDP900Channel(fConnection, channel));
    }
}

RigolDP900Channel::RigolDP900Channel(Connection* connection, const pugi::xml_node configuration) : PowerSupplyChannel(configuration), fConnection(connection),fChannel(fConfiguration.attribute("Channel").value())
{
}
RigolDP900Channel::~RigolDP900Channel() {}

/*!
************************************************
 * Sends write command to connection.
 \param command Command to be send.
************************************************
*/
void RigolDP900Channel::write(std::string command)
{
    fConnection->write(command);
}

/*!
************************************************
 * Sends read command to connection.
 \param command Command to be send.
************************************************
*/
std::string RigolDP900Channel::read(std::string command)
{
    std::string answer = fConnection->read(command);
    int i = answer.find("\n");
    if ( i >= 0) {
	    return  answer.substr(0,i);
    }
    return answer;
}

void RigolDP900Channel::turnOn()
{
    write(std::string(":OUTP ")+fChannel+",ON");
    // std::cout << "Turn on channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

void RigolDP900Channel::turnOff()
{
    write(std::string(":OUTP ")+fChannel+",OFF");
    // std::cout << "Turn off channel " << fConfiguration.attribute("Channel").value() << " output." << std::endl;
}

bool RigolDP900Channel::isOn()
{
    std::string answer = read("OUTP? " + fChannel);
    std::cout<< answer.substr(0,2) << std::endl;
    return answer.substr(0,2) == "ON";
}

void RigolDP900Channel::setVoltage(float voltage) 
{ 
	write(":APPL " + fChannel + "," + std::to_string(voltage)); 
}

void RigolDP900Channel::setCurrent(float current) 
{ 
    setCurrentCompliance(current); 
}

float RigolDP900Channel::getOutputVoltage()
{
  std::string answer = read(":MEAS:VOLT? "+fChannel);
  float       result=0;
  if(answer.rfind("OFF") == std::string::npos) {
   sscanf(answer.c_str(), "%f", &result);
  }
  return result;
}

float RigolDP900Channel::getSetVoltage() 
{ 
    return getVoltageCompliance(); 
}

float RigolDP900Channel::getCurrent()
{
  std::string answer = read(":MEAS:CURR? "+fChannel);
  float       result=0;
  if(answer.rfind("OFF") == std::string::npos) {
   sscanf(answer.c_str(), "%f", &result);
  }
  return result;
}

void RigolDP900Channel::setVoltageCompliance(float voltage) 
{ 
    setVoltage(voltage); 
}

void RigolDP900Channel::setCurrentCompliance(float current) 
{
	write(":APPL "+fChannel+"," + std::to_string(getSetVoltage())+","+ std::to_string(current)); 
}

float RigolDP900Channel::getVoltageCompliance()
{
  std::string answer = read(":APPL? "+fChannel+",VOLT");
  float       result=0;
  if(answer.rfind("OFF") == std::string::npos) {
   sscanf(answer.c_str(), "%f", &result);
  }
  return result;
}

float RigolDP900Channel::getCurrentCompliance()
{
  std::string answer = read(":APPL? "+fChannel+",CURR");
  float       result=0;
  if(answer.rfind("OFF") == std::string::npos) {
   sscanf(answer.c_str(), "%f", &result);
  }
  return result;
}

void RigolDP900Channel::setOverVoltageProtection(float maxVoltage) { 
	write(":OUTP:OVP:VAL " + fChannel + ","+ std::to_string(maxVoltage)); 
}

void RigolDP900Channel::setOverCurrentProtection(float maxCurrent)
{
	write(":OUTP:OVC:VAL " + fChannel + ","+ std::to_string(maxCurrent)); 
}

float RigolDP900Channel::getOverVoltageProtection()
{
  std::string answer = read(":OUTP:OVP:VAL? "+fChannel);
  float       result=0;
  if(answer.rfind("OFF") == std::string::npos) {
   sscanf(answer.c_str(), "%f", &result);
  }
  return result;
}

float RigolDP900Channel::getOverCurrentProtection()
{
  std::string answer = read(":OUTP:OVC:VAL? "+fChannel);
  float       result=0;
  if(answer.rfind("OFF") == std::string::npos) {
   sscanf(answer.c_str(), "%f", &result);
  }
  return result;
}

void RigolDP900Channel::setParameter(std::string parName, float value) { write(parName + " " + std::to_string(value)); }

void RigolDP900Channel::setParameter(std::string parName, bool value) { write(parName + " " + std::to_string(value)); }

void RigolDP900Channel::setParameter(std::string parName, int value) { write(parName + " " + std::to_string(value)); }

float RigolDP900Channel::getParameterFloat(std::string parName)
{
    std::string answer = read(parName);
    float       result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

int RigolDP900Channel::getParameterInt(std::string parName)
{
    std::string answer = read(parName);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

bool RigolDP900Channel::getParameterBool(std::string parName)
{
    std::string answer = read(parName);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

