/*!
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Dec 17 2021
 */
#include "Tools.h"

/*!
************************************************
* Class constructor for generic tool class.
************************************************
*/

Tools::Tools() {}

/*! \brief A simple "wait for input".
************************************************
* A simple "wait for input".
************************************************
*/
void Tools::Wait()
{
    std::cout << "Press ENTER to continue...";
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

/*! \brief "Yes or No" handler.
************************************************
* A simple "Yes or No" input handler.
************************************************
*/
bool Tools::AskForYesOrNo(std::string question)
{
    std::string ans;
    std::cout << question << " [Yn]" << std::endl;
    while(std::getline(std::cin, ans))
    {
        if(ans.empty() || ans == "y" || ans == "Y")
            return true;
        else if(ans == "n" || ans == "N")
            return false;
        else
            std::cout << ans << " wrong option, choose among [Yn]" << std::endl;
    }
    return false;
}

/*! \brief Wiat for specific input
************************************************
* Wait for specific string input.
************************************************
*/
bool Tools::AskForSpecificInput(std::string question, std::string expectedAnswer)
{
    std::string ans;
    std::cout << question << " " << expectedAnswer << " n otherwise" << std::endl;
    while(std::getline(std::cin, ans))
    {
        if(ans == expectedAnswer)
            return true;
        else if(ans == "n" || ans == "N")
            return false;
        else
            std::cout << ans << " wrong option, choose among " << expectedAnswer << " and n" << std::endl;
    }
    return false;
}

/*! \brief Clean strings.
************************************************
*  Cleans string from child_value from
*  unwanted characters (as spaces).
* \param strClean String to be clean.
* \return Cleaned string.
************************************************
*/
std::string Tools::ChildValClean(std::string strClean)
{
    boost::trim(strClean);
    return strClean;
}

/*!
************************************************
 * Creates scanner card map between channel
 * number and "names".
 \param configNode xml node with informations.
 \param channelMap std::map for map creation
 storage.
 \param verbose Verbosity.
 \return Returns false (method not
 implemented).
************************************************
*/

bool Tools::CreateScannerCardMap(pugi::xml_node* configNode, std::map<int, std::string>& channelMap, bool verbose)
{
    if(verbose) std::cout << "Creating map for multimeter: " << std::endl;
    std::ostringstream  searchChannelStr;
    pugi::xml_attribute attr;
    int                 vChannel;
    for(pugi::xml_attribute attr = configNode->first_attribute(); attr; attr = attr.next_attribute())
    {
        if(std::sscanf(attr.name(), "Channel_%d", &vChannel) != 1) continue;
        if(verbose) std::cout << "Channel map emplace vChannel: " << vChannel << ", attribute: " << attr.value() << std::endl;
        channelMap.emplace(vChannel, attr.value());
    }
    return true;
}
