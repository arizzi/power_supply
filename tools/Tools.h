/*!
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Dec 17 2021
 */

#ifndef Tools_H
#define Tools_H

#include <boost/algorithm/string.hpp>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <pugixml.hpp>
#include <sstream>
#include <string>
#include <vector>

/*!
************************************************
 \class Tools.
 \brief Generic tools.
************************************************
*/

class Tools
{
  public:
    Tools();
    void        Wait();
    std::string ChildValClean(std::string);
    bool        AskForYesOrNo(std::string);
    bool        AskForSpecificInput(std::string, std::string);
    bool        CreateScannerCardMap(pugi::xml_node*, std::map<int, std::string>&, bool verbose = false);

  private:
};

#endif
